json = require("jsonInterface")

local Morrowind = os.getenv("MOD_DIR") .. "/items/Morrowind.json"
local Tribunal = os.getenv("MOD_DIR") .. "/items/Tribunal.json"
local Bloodmoon = os.getenv("MOD_DIR") .. "/items/Bloodmoon.json"
local FL = os.getenv("MOD_DIR") .. "/items/FL.json"

Methods = {}

function Methods.RetriveItemName(refid)
	if Morrowind[refid] ~= nil then
		return Morrowind[refid]
	elseif Tribunal[refid] ~= nil then
		return Tribunal[refid]
	elseif Bloodmoon[refid] ~= nil then
		return Bloodmoon[refid]
	elseif FL[refid] ~= nil then
		return FL[refid]
	else
		return refid
	end
end

return Methods
