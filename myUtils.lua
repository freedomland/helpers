--[[
	Library for common actions that doesn't exist in vanila Core Scripts
]]--

Methods = {}

-- Find ID in specific cell and return numMp
function Methods.FindId(pid, cell, id)
	if LoadedCells[cell] ~= nil then
		for i, t in pairs(LoadedCells[cell].data.objectData) do		
			if t.refId == id then
				return i
			end
		end
	end
	
	return nil
end

-- Find journal entry
function Methods.FindJournal(pid, id)
	for i, t in pairs(Players[pid].data.journal) do
		if t.quest == id then
			return true
		end
	end
	
	return false
end

-- Find last journal entry and return current index, if nothing found return -1
function Methods.FindJournalIndex(pid, id)
	local ret = -1
	
	for i, t in pairs(Players[pid].data.journal) do
		if t.quest == id then
			ret = t.index
		end
	end
	
	return ret
end

-- In opposite to myMod function, create object for ALL players currently login in server
function Methods.CreateObjectForPlayers(cell, location, refId, packetType)
    local mpNum = WorldInstance:GetCurrentMpNum() + 1
    local refIndex =  0 .. "-" .. mpNum

	tes3mp.InitializeEvent(tableHelper.getAnyValue(Players).pid)
	tes3mp.SetEventCell(cell)
    tes3mp.SetObjectRefId(refId)
    tes3mp.SetObjectRefNumIndex(0)
    tes3mp.SetObjectMpNum(mpNum)
    tes3mp.SetObjectPosition(location.posX, location.posY, location.posZ)
    tes3mp.SetObjectRotation(location.rotX, location.rotY, location.rotZ)
    tes3mp.AddWorldObject()

    if packetType == "place" then
		tes3mp.SendObjectPlace(true)
	elseif packetType == "spawn" then
		tes3mp.SendObjectSpawn(true)
	end
end

return Methods
