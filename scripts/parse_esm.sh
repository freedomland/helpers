# This script will parse ESM files and generate JSON for your lua mods
# How to use: put this script in OpenMW or install OpenMW with your distribution
# package manager, run like this: ./parse_esm.sh "path_to_esm" it will soon generate records.json file
# Note, that not all fields for now parse correctly
#
# *  This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# * */

#!/bin/sh

# https://github.com/OpenMW/openmw/blob/master/apps/esmtool/esmtool.cpp#L38
FIELDS="MISC;WEAP;CONT;CREA;NPC_;ARMO;CLOT;REPA;APPA;LOCK;PROB;INGR;BOOK;ALCH" #DIAL
# LIGH are problematic to parse right now, so it disabled
# You also can add DIAL field to this, but you'll need to parse it yourself

echo "{" > records

for i in $(seq 1 14); do
	# we DO need to set encoding to win1251, because else we'll get garbage on UTF-8 strings
	esmtool -e win1251 dump "$1" -t $(echo $FIELDS | cut -d ';' -f$i) | egrep "(Record: | Name: )" >> records.json
done

# Remove all Male and Female names, we don't realy need them
sed -i '/Male Name/d' records.json
sed -i '/Female Name/d' records.json

# Remove labels
sed -i 's/Name://g' records.json
sed -i 's/Record://g' records.json

for i in $(seq 1 17); do
	sed -i 's/'$(echo $FIELDS | cut -d ';' -f$i)'//g' records.json
done

# Change three space to :"
sed -i 's/   /:"/g' records.json

# Change two spaces and ' to "
#sed -i "s/  '/\"/g" records.json

# Change remaining ' to ", it will broke some thing, but, nuh, who cares
sed -i "s/'/\"/g" records.json

# This shit will remove newline
sed -i ':a;N;$!ba;s/"\n/"/g' records.json
sed -i ':a;N;$!ba;s/\n/",\n/g' records.json

echo "\"}" >> records.json
