/*
 * getRandomSeed.so (c) 2018 Freedom Land Team, GPL v3.0
 * 
 * Return random seed from /dev/urandom directly to Lua
 * Only works on *NIX!
 * 
 * How to compile:
 * gcc getLinuxSeed.c -lluajit-5.1 -shared -fPIC -o getLinuxSeed.so
 * 
 * How to use:
 * require("getLinuxSeed")
	math.randomseed(getLinuxSeed())
	print(math.random(1, 1000))
 * 
 * DO NOT use for cryptography or such things.
 * 
 *  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * */

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/random.h>
#include "luajit-2.0/lua.h"
#include "luajit-2.0/lualib.h"
#include "luajit-2.0/lauxlib.h"

static int getLinuxSeed(lua_State *L)
{
	int s;
	syscall(SYS_getrandom, &s, sizeof(int), 0);
	
	lua_pushnumber(L, s);
	
	return 1;
}

int luaopen_getLinuxSeed(lua_State *L)
{
	lua_register(L, "getLinuxSeed", getLinuxSeed);
	return 0;
}
